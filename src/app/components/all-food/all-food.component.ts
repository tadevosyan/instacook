import { Component, OnInit } from '@angular/core';


declare var $;
declare var jQuery;

@Component({
  selector: 'app-all-food',
  templateUrl: './all-food.component.html',
  styleUrls: ['./all-food.component.css']
})
export class AllFoodComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }



  firstRecipe(){
    $(".content").slideUp(500);
    $(".buttons").slideUp(500);
    $(".first-recipe").slideDown(1500);

  }
  secondRecipe(){
    $(".content").slideUp(500);
    $(".buttons").slideUp(500);
    $(".second-recipe").slideDown(1500);
  }
  thirdRecipe(){
    $(".content").slideUp(500);
    $(".buttons").slideUp(500);
    $(".third-recipe").slideDown(1500);
  }
  closeRecipe() {
    $(".first-recipe").slideUp(500);
    $(".second-recipe").slideUp(500);
    $(".third-recipe").slideUp(500);
    $(".content").slideDown(1500);
    $(".buttons").slideDown(1000);
  }
}
