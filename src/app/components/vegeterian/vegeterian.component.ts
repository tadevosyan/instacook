import { Component, OnInit } from '@angular/core';

declare var  $;
declare var jQuery;


@Component({
  selector: 'app-vegeterian',
  templateUrl: './vegeterian.component.html',
  styleUrls: ['./vegeterian.component.css']
})
export class VegeterianComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }



  firstRecipe(){
    $(".content").slideUp(500);
    $(".buttons").slideUp(500);
    $(".first-recipe").slideDown(1500);

  }
  secondRecipe(){
    $(".content").slideUp(500);
    $(".buttons").slideUp(500);
    $(".second-recipe").slideDown(1500);
  }
  thirdRecipe(){
    $(".content").slideUp(500);
    $(".buttons").slideUp(500);
    $(".third-recipe").slideDown(1500);
  }
  closeRecipe() {
    $(".first-recipe").slideUp(500);
    $(".second-recipe").slideUp(500);
    $(".third-recipe").slideUp(500);
    $(".content").slideDown(1500);
    $(".buttons").slideDown(1000);
  }

}
