import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MnFullpageModule } from "ngx-fullpage";
import 'fullpage.js';
import { HomeComponent } from './components/home/home.component';
import { VeganComponent } from './components/vegan/vegan.component';
import { VegeterianComponent } from './components/vegeterian/vegeterian.component';
import { AllFoodComponent } from './components/all-food/all-food.component';
 

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    VeganComponent,
    AllFoodComponent,
    VegeterianComponent
  ],
  imports: [
    BrowserModule,
    MnFullpageModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
